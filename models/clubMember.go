package models

import "github.com/google/uuid"

type (
	ClubMember struct {
		Id         uuid.UUID `gorm:"primary_key; unique" json:"id"`
		Fullname   string    `gorm:"type:varchar(255);not null" json:"fullname" validate:"omitempty,min=1,max=255"`
		Phone      int       `gorm:"type:int;not null" json:"phone" validate:"omitempty"`
		PictureURL string    `gorm:"type:text;not null" json:"picture_url" validate:"omitempty"`

		// relation
		Club   Club      `gorm:"foreignkey:ClubId"`
		ClubId uuid.UUID `gorm:"index"`
	}

	ClubMemberInput struct {
		Fullname   string `json:"fullname" binding:"required"`
		Phone      int    `json:"phone" binding:"required"`
		PictureURL string `json:"picture_url" binding:"required"`
	}
)
