package models

import (
	"time"

	"github.com/google/uuid"
)

type (
	Event struct {
		Id          uuid.UUID `gorm:"primary_key; unique" json:"id"`
		EventName   string    `gorm:"type:varchar(255);unique;not null" json:"event_name" validate:"omitempty,min=1,max=255"`
		Host        string    `gorm:"type:varchar(255);not null" json:"host" validate:"omitempty,min=1,max=255"`
		Time        time.Time `gorm:"not null" json:"time" validate:"omitempty"`
		City        string    `gorm:"type:varchar(255);not null" json:"city" validate:"omitempty,min=1,max=255"`
		Province    string    `gorm:"type:varchar(255);not null" json:"province" validate:"omitempty,min=1,max=255"`
		Fee         int       `gorm:"type:int;not null" json:"fee" validate:"omitempty"`
		Email       string    `gorm:"type:varchar(255);not null" json:"email" validate:"omitempty,email,min=1,max=255"`
		Phone       int       `gorm:"type:int;not null" json:"phone" validate:"omitempty"`
		PosterUrl   string    `gorm:"type:text;not null" json:"poster_url" validate:"omitempty"`
		Description string    `gorm:"type:text;not null" json:"description" validate:"omitempty"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`

		// relation
		Admin   Admin     `gorm:"foreignkey:AdminID"`
		AdminID uuid.UUID `gorm:"index"`

		SportCategory   SportCategory `gorm:"foreignkey:SportCategoryID"`
		SportCategoryID uuid.UUID     `gorm:"index"`

		Venue   Venue     `gorm:"foreignkey:VenueID"`
		VenueID uuid.UUID `gorm:"index"`

		EventCart []EventCart
	}

	EventInput struct {
		EventName       string    `json:"event_name" binding:"required"`
		Host            string    `json:"host" binding:"required"`
		Time            string    `json:"time" binding:"required"`
		City            string    `json:"city" binding:"required"`
		Province        string    `json:"province" binding:"required"`
		Fee             int       `json:"fee" binding:"required"`
		Email           string    `json:"email" binding:"required"`
		Phone           int       `json:"phone" binding:"required"`
		PosterUrl       string    `json:"poster_url" binding:"required"`
		Description     string    `json:"description" binding:"required"`
		SportCategoryID uuid.UUID `json:"sport_category_id" binding:"required"`
		VenueID         uuid.UUID `json:"venue_id" binding:"required"`
	}
)
