package models

import (
	"sparingapp/utils/token"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type (
	// Database Model for Club
	Club struct {
		Id        uuid.UUID `gorm:"primary_key; unique" json:"id"`
		ClubName  string    `gorm:"type:varchar(255);unique;not null" json:"club_name" validate:"omitempty,min=1,max=255"`
		Email     string    `gorm:"type:varchar(255);not null" json:"email" validate:"omitempty,email,min=1,max=255"`
		Password  string    `gorm:"type:varchar(255);not null" json:"password" validate:"omitempty,min=6,max=255"`
		Phone     int       `gorm:"type:int;not null" json:"phone" validate:"omitempty"`
		Manager   string    `gorm:"type:varchar(255);not null" json:"manager" validate:"omitempty,min=1,max=255"`
		City      string    `gorm:"type:varchar(255);not null" json:"city" validate:"omitempty,min=1,max=255"`
		Province  string    `gorm:"type:varchar(255);not null" json:"province" validate:"omitempty,min=1,max=255"`
		LogoUrl   string    `gorm:"type:text;not null" json:"logo_url" validate:"omitempty"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`

		// relation
		SportCategory   SportCategory `gorm:"foreignkey:SportCategoryId"`
		SportCategoryId uuid.UUID     `gorm:"index"`

		ClubMember []ClubMember

		EventCart []EventCart
	}

	// Input Model for Register Club
	ClubRegisterInput struct {
		ClubName        string    `json:"club_name" binding:"required"`
		Email           string    `json:"email" binding:"required"`
		Password        string    `json:"password" binding:"required"`
		Phone           int       `json:"phone" binding:"required"`
		Manager         string    `json:"manager" binding:"required"`
		City            string    `json:"city" binding:"required"`
		Province        string    `json:"province" binding:"required"`
		LogoUrl         string    `json:"logo_url" binding:"required"`
		SportCategoryId uuid.UUID `json:"sport_category_id" binding:"required"`
	}

	// Input Model for Login Club
	ClubLoginInput struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
)

// VerifyPassword checks if password is correct
func ClubVerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func ClubLoginCheck(email string, password string, db *gorm.DB) (string, error) {
	var err error

	c := Club{}

	// check if email exists
	err = db.Model(c).Where("email = ?", email).Take(&c).Error
	// if not found return error
	if err != nil {
		return "", err
	}

	// check if password is correct
	err = ClubVerifyPassword(password, c.Password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	// Generating token for Club
	token, err := token.GenerateToken(c.Id)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (c *Club) SaveClub(db *gorm.DB) (*Club, error) {
	// Change password into hash
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(c.Password), bcrypt.DefaultCost)
	if err != nil {
		return &Club{}, err
	}

	// generate UUID
	c.Id = uuid.New()

	// set password to hash
	c.Password = string(hashedPassword)

	// create user data to database
	err = db.Create(&c).Error
	if err != nil {
		return &Club{}, err
	}

	return c, nil
}
