package models

import (
	"time"

	"github.com/google/uuid"
)

type (
	Venue struct {
		Id        uuid.UUID `gorm:"primary_key; unique" json:"id"`
		VenueName string    `gorm:"type:varchar(255);not null" json:"venue_name" validate:"omitempty,min=1,max=255"`
		ImageUrl  string    `gorm:"type:text;not null" json:"image_url" validate:"omitempty"`
		Facility  string    `gorm:"type:text;not null" json:"facility" validate:"omitempty"`
		City      string    `gorm:"type:varchar(255);not null" json:"city" validate:"omitempty,min=1,max=255"`
		Province  string    `gorm:"type:varchar(255);not null" json:"province" validate:"omitempty,min=1,max=255"`
		Price     int       `gorm:"type:int;not null" json:"price" validate:"omitempty"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`

		// relation
		Admin   Admin     `gorm:"foreignkey:AdminID" validate:"omitempty"`
		AdminID uuid.UUID `gorm:"index"`

		SportCategory   SportCategory `gorm:"foreignkey:SportCategoryID"`
		SportCategoryID uuid.UUID     `gorm:"index"`

		Event []Event
	}

	VenueInput struct {
		VenueName       string    `json:"venue_name" binding:"required"`
		ImageUrl        string    `json:"image_url" binding:"required"`
		Facility        string    `json:"facility" binding:"required"`
		City            string    `json:"city" binding:"required"`
		Province        string    `json:"province" binding:"required"`
		Price           int       `json:"price" binding:"required"`
		SportCategoryID uuid.UUID `json:"sport_category_id" binding:"required"`
	}
)
