package models

import "github.com/google/uuid"

type (
	EventCart struct {
		Id uuid.UUID `gorm:"primary_key; unique" json:"id"`

		// relation
		Event   Event     `gorm:"foreignkey:EventID"`
		EventID uuid.UUID `gorm:"index"`

		Club   Club      `gorm:"foreignkey:ClubId"`
		ClubId uuid.UUID `gorm:"index"`
	}

	EventCartInput struct {
		EventID uuid.UUID `json:"event_id" binding:"required"`
	}
)
