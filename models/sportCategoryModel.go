package models

import "github.com/google/uuid"

type (
	SportCategory struct {
		Id       uuid.UUID `gorm:"primary_key; unique" json:"id"`
		Category string    `gorm:"type:varchar(255);unique;not null" json:"category" validate:"omitempty,min=1,max=255"`

		// relation
		Venue []Venue

		Event []Event

		Club []Club
	}

	SportCategoryInput struct {
		Category string `json:"category" binding:"required"`
	}
)
