package models

import (
	"sparingapp/utils/token"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type (
	// Database Model for Admin
	Admin struct {
		Id        uuid.UUID `gorm:"primary_key; unique" json:"id"`
		Username  string    `gorm:"type:varchar(255);unique;not null" json:"username" validate:"omitempty,min=3,max=255"`
		Fullname  string    `gorm:"type:varchar(255);not null" json:"fullname" validate:"omitempty,min=3,max=255"`
		Email     string    `gorm:"type:varchar(255);unique;not null" json:"email" validate:"omitempty,email,min=3,max=255"`
		Password  string    `gorm:"type:varchar(255);not null" json:"password" validate:"omitempty,min=6,max=255"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`

		VenueID []Venue
	}

	// Input Model for Register Admin
	AdminRegisterInput struct {
		Username string `json:"username" binding:"required"`
		Fullname string `json:"fullname" binding:"required"`
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	// Input Model for Login Admin
	AdminLoginInput struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
)

// VerifyPassword checks if password is correct
func AdminVerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func AdminLoginCheck(email string, password string, db *gorm.DB) (string, error) {
	var err error

	a := Admin{}

	// check if email exists
	err = db.Model(a).Where("email = ?", email).Take(&a).Error
	// if not found return error
	if err != nil {
		return "", err
	}

	// check if password is correct
	err = AdminVerifyPassword(password, a.Password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	// Generating token for admin
	token, err := token.GenerateToken(a.Id)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (a *Admin) SaveAdmin(db *gorm.DB) (*Admin, error) {
	// Change password into hash
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(a.Password), bcrypt.DefaultCost)
	if err != nil {
		return &Admin{}, err
	}

	// generate UUID
	a.Id = uuid.New()

	// set password to hash
	a.Password = string(hashedPassword)

	// create user data to database
	err = db.Create(&a).Error
	if err != nil {
		return &Admin{}, err
	}

	return a, nil
}
