package controllers

import (
	"net/http"
	"sparingapp/models"
	"sparingapp/utils/token"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func GetAllVenue(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// gorm : get all data from venue table
	var venues []models.Venue
	db.Set("gorm:auto_preload", true).Preload("SportCategory").Find(&venues)

	// return all venue data
	c.JSON(http.StatusOK, gin.H{"venue": venues})
}

func GetVenueById(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// get single venue data by param id
	var venue []models.Venue
	err := db.Set("gorm:auto_preload", true).Preload("SportCategory").First(&venue, "id = ?", c.Param("id")).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// return the data of venue
	c.JSON(http.StatusOK, gin.H{"data": venue})
}

func PostVenue(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.VenueInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Extract Token ID
	id, _ := token.ExtractTokenID(c)

	// Input Data venue into a Struct
	venue := models.Venue{
		Id:              uuid.New(),
		VenueName:       input.VenueName,
		ImageUrl:        input.ImageUrl,
		Facility:        input.Facility,
		City:            input.City,
		Province:        input.Province,
		Price:           input.Price,
		AdminID:         id,
		SportCategoryID: input.SportCategoryID,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(&venue)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// input venue data to database
	err = db.Create(&venue).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	// return venue data
	c.JSON(http.StatusOK, gin.H{"venue": venue})
}

func EditVenue(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// extract token to get admin ID
	adminId, _ := token.ExtractTokenID(c)

	// Get model if data exist
	var venue models.Venue
	err := db.Where("Id = ?", c.Param("id")).First(&venue).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// Input Validation Format in JSON
	var input models.VenueInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Update Data Venue
	venue.VenueName = input.VenueName
	venue.ImageUrl = input.ImageUrl
	venue.Facility = input.Facility
	venue.City = input.City
	venue.Province = input.Province
	venue.Price = input.Price
	venue.AdminID = adminId
	venue.SportCategoryID = input.SportCategoryID

	// Validation using validator v10
	validate := validator.New()
	err = validate.Struct(&venue)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// update data into database
	err = db.Model(&venue).Updates(venue).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"venue": venue})
}

func DeleteVenue(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// get model if data exist
	var venue models.Venue
	err := db.Where("Id = ?", c.Param("id")).First(&venue).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// delete data in the database
	db.Delete(&venue)

	c.JSON(http.StatusOK, gin.H{"message": "Venue has been deleted"})
}
