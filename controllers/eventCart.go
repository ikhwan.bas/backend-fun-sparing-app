package controllers

import (
	"net/http"
	"sparingapp/models"
	"sparingapp/utils/token"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func PostCart(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// extract token to get club id
	ClubId, _ := token.ExtractTokenID(c)

	// Input Validation Format in JSON
	var input models.EventCartInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Cart into a Struct
	cart := models.EventCart{
		Id:      uuid.New(),
		EventID: input.EventID,
		ClubId:  ClubId,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(cart)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// input cart data to database
	err = db.Create(&cart).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return cart data
	c.JSON(http.StatusOK, gin.H{"eventId": cart.EventID, "clubId": cart.ClubId})
}

func GetCartByTokenId(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// extract token to get club id
	ClubId, _ := token.ExtractTokenID(c)

	// get all cart data by token id
	var eventCart []models.EventCart
	err := db.Set("gorm:auto_preload", true).Preload("Event").Preload("Club").Find(&eventCart, "club_id = ?", ClubId).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// return the data of cart
	c.JSON(http.StatusOK, gin.H{"data": eventCart})
}

func GetCartByParamEventId(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// get all cart data by token id
	var eventCart []models.EventCart
	err := db.Set("gorm:auto_preload", true).Preload("Event").Preload("Club").Find(&eventCart, "event_id = ?", c.Param("id")).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// return the data of cart
	c.JSON(http.StatusOK, gin.H{"data": eventCart})
}

func DeleteCart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// extract token to get club id
	ClubId, _ := token.ExtractTokenID(c)

	// get model if data exist
	var cart models.EventCart
	err := db.Where("event_id = ? AND club_id = ?", c.Param("id"), ClubId).First(&cart).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// delete data in the database
	db.Delete(&cart)

	c.JSON(http.StatusOK, gin.H{"message": "Cart has been deleted"})
}
