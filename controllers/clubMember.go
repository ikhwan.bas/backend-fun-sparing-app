package controllers

import (
	"net/http"
	"sparingapp/models"
	"sparingapp/utils/token"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func PostClubMember(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.ClubMemberInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Extract Token ID Club
	clubId, _ := token.ExtractTokenID(c)

	// Input Data Club Member into a Struct
	clubMember := models.ClubMember{
		Id:         uuid.New(),
		Fullname:   input.Fullname,
		Phone:      input.Phone,
		PictureURL: input.PictureURL,
		ClubId:     clubId,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(&clubMember)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// input club member data to database
	err = db.Create(&clubMember).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return event data
	c.JSON(http.StatusOK, gin.H{"clubmember": clubMember})
}

func DeleteClubMember(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// get model if data exist
	var clubMember models.ClubMember
	err := db.Where("Id = ?", c.Param("id")).First(&clubMember).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// delete data in the database
	db.Delete(&clubMember)

	c.JSON(http.StatusOK, gin.H{"message": "Member has been removed"})
}

func EditClubMember(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if data exist
	var clubMember models.ClubMember
	err := db.Where("Id = ?", c.Param("id")).First(&clubMember).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// Input Validation Format in JSON
	var input models.ClubMember
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Update Data Club Member
	clubMember.Fullname = input.Fullname
	clubMember.Phone = input.Phone
	clubMember.PictureURL = input.PictureURL

	// Validation using validator v10
	validate := validator.New()
	err = validate.Struct(&clubMember)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// update data into database
	err = db.Model(&clubMember).Updates(clubMember).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"club_member": clubMember})
}
