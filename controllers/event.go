package controllers

import (
	"fmt"
	"net/http"
	"sparingapp/models"
	"sparingapp/utils/token"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

const layout = "2006-01-02"

func GetAllEvents(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// gorm : get all data from event table
	var events []models.Event
	db.Set("gorm:auto_preload", true).Preload("SportCategory").Find(&events)

	// return all events data
	c.JSON(http.StatusOK, gin.H{"events": events})
}

func GetEventById(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// get single event data by param id
	var event []models.Event
	err := db.Set("gorm:auto_preload", true).Preload("SportCategory").Preload("Venue").First(&event, "id = ?", c.Param("id")).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// return the data of event
	c.JSON(http.StatusOK, gin.H{"data": event})
}

func PostEvents(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.EventInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Extract Token ID Admin
	adminId, _ := token.ExtractTokenID(c)

	formatTime, _ := time.Parse(layout, input.Time)
	fmt.Println(formatTime)
	// Input Data venue into a Struct
	event := models.Event{
		Id:              uuid.New(),
		EventName:       input.EventName,
		Host:            input.Host,
		Time:            formatTime,
		City:            input.City,
		Province:        input.Province,
		Fee:             input.Fee,
		Email:           input.Email,
		Phone:           input.Phone,
		PosterUrl:       input.PosterUrl,
		Description:     input.Description,
		SportCategoryID: input.SportCategoryID,
		VenueID:         input.VenueID,
		AdminID:         adminId,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(&event)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// input event data to database
	err = db.Create(&event).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return event data
	c.JSON(http.StatusOK, gin.H{"event": event})
}

func DeleteEvents(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// get model if data exist
	var event models.Event
	err := db.Where("Id = ?", c.Param("id")).First(&event).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// delete data in the database
	db.Delete(&event)

	c.JSON(http.StatusOK, gin.H{"message": "Venue has been deleted"})
}

// ##### incoming features #####

// func EditEvents(c *gin.Context) {
// 	db := c.MustGet("db").(*gorm.DB)

// 	// extract token to get admin ID
// 	adminId, _ := token.ExtractTokenID(c)

// 	// Get model if data exist
// 	var venue models.Venue
// 	err := db.Where("Id = ?", c.Param("id")).First(&venue).Error
// 	if err != nil {
// 		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
// 		return
// 	}

// 	// Input Validation Format in JSON
// 	var input models.VenueInput
// 	if err := c.ShouldBindJSON(&input); err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// 		return
// 	}

// 	// Update Data Venue
// 	venue.VenueName = input.VenueName
// 	venue.ImageUrl = input.ImageUrl
// 	venue.Facility = input.Facility
// 	venue.City = input.City
// 	venue.Province = input.Province
// 	venue.Price = input.Price
// 	venue.AdminID = adminId
// 	venue.SportCategoryID = input.SportCategoryID

// 	// Validation using validator v10
// 	validate := validator.New()
// 	err = validate.Struct(&venue)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// 		return
// 	}

// 	// update data into database
// 	err = db.Model(&venue).Updates(venue).Error
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// 		return
// 	}

// 	c.JSON(http.StatusOK, gin.H{"venue": venue})
// }
