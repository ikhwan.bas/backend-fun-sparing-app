package controllers

import (
	"net/http"
	"sparingapp/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func PostCategory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.SportCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Category into a Struct
	category := models.SportCategory{
		Id:       uuid.New(),
		Category: input.Category,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(category)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// input category data to database
	err = db.Create(&category).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	// return category data
	c.JSON(http.StatusOK, gin.H{"category": category.Category})
}

func GetAllCategory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// gorm : get all data from category table
	var categories []models.SportCategory
	db.Find(&categories)

	// return all category data
	c.JSON(http.StatusOK, gin.H{"categories": categories})
}
