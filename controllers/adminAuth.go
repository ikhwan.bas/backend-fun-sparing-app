package controllers

import (
	"net/http"
	"sparingapp/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func RegisterAdmin(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.AdminRegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Admin into a Struct
	admin := models.Admin{
		Username: input.Username,
		Fullname: input.Fullname,
		Email:    input.Email,
		Password: input.Password,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(admin)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// calling method Save Admin
	_, err = admin.SaveAdmin(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return admin data
	c.JSON(http.StatusOK, gin.H{"username": admin.Username, "fullname": admin.Fullname, "email": admin.Email})
}

func LoginAdmin(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.AdminLoginInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Login Admin into a Struct
	admin := models.Admin{
		Email:    input.Email,
		Password: input.Password,
	}

	// calling function to check email and password in database
	token, err := models.AdminLoginCheck(admin.Email, admin.Password, db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	// return token if login success
	c.JSON(http.StatusOK, gin.H{"message": "login success", "token": token})

}
