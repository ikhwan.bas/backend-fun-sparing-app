package controllers

import (
	"net/http"
	"sparingapp/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func RegisterClub(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.ClubRegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Club into a Struct
	club := models.Club{
		ClubName:        input.ClubName,
		Email:           input.Email,
		Password:        input.Password,
		Phone:           input.Phone,
		Manager:         input.Manager,
		City:            input.City,
		Province:        input.Province,
		LogoUrl:         input.LogoUrl,
		SportCategoryId: input.SportCategoryId,
	}

	// Validation using validator v10
	validate := validator.New()
	err := validate.Struct(club)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// calling method Save Club
	_, err = club.SaveClub(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// return club data
	c.JSON(http.StatusOK, gin.H{"club_name": club.ClubName, "email": club.Email, "manager": club.Manager})
}

func LoginClub(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Input Validation Format in JSON
	var input models.ClubLoginInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Input Data Login Club into a Struct
	club := models.Club{
		Email:    input.Email,
		Password: input.Password,
	}

	// calling function to check email and password in database
	token, err := models.ClubLoginCheck(club.Email, club.Password, db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	// return token if login success
	c.JSON(http.StatusOK, gin.H{"message": "login success", "token": token})
}
