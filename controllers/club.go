package controllers

import (
	"net/http"
	"sparingapp/models"
	"sparingapp/utils/token"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func GetClubById(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// Extract Token to get club ID
	clubId, _ := token.ExtractTokenID(c)

	// get single club data by token id
	var club []models.Club
	err := db.Set("gorm:auto_preload", true).Preload("ClubMember").Where("id = ?", clubId).First(&club).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}
	// return the data of club and club member
	c.JSON(http.StatusOK, gin.H{"data": club})
}

func EditClubProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Extract Token to get club ID
	clubId, _ := token.ExtractTokenID(c)

	// Get model if data exist
	var club models.Club
	err := db.Where("Id = ?", clubId).First(&club).Error
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	// Input Validation Format in JSON
	var input models.Club
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Update Data Club
	club.ClubName = input.ClubName
	club.Email = input.Email
	club.Phone = input.Phone
	club.Manager = input.Manager
	club.City = input.City
	club.Province = input.Province
	club.LogoUrl = input.LogoUrl

	// Validation using validator v10
	validate := validator.New()
	err = validate.Struct(&club)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// update data into database
	err = db.Model(&club).Updates(club).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"club": club})
}
