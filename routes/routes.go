package routes

import (
	"sparingapp/controllers"
	"sparingapp/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies([]string{"localhost"})

	// CORS Setting
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true
	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")
	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// JWT Setup
	jwt := r.Group("/")
	jwt.Use(middlewares.JwtAuthMiddleware())

	// ROUTES SETUP

	// Admin Routes
	r.POST("/admin/register", controllers.RegisterAdmin)
	r.POST("/admin/login", controllers.LoginAdmin)

	// Club Routes
	r.POST("/club/register", controllers.RegisterClub)
	r.POST("/club/login", controllers.LoginClub)
	jwt.GET("/club/profile", controllers.GetClubById)
	jwt.PUT("/club/profile", controllers.EditClubProfile)

	// Club Member Routes
	jwt.POST("/club/member", controllers.PostClubMember)
	jwt.DELETE("/club/member/:id", controllers.DeleteClubMember)
	jwt.PUT("/club/member/:id", controllers.EditClubMember)

	// Category Routes
	jwt.POST("/sports/category", controllers.PostCategory)
	r.GET("/sports/category", controllers.GetAllCategory)

	// Venue Routes
	r.GET("/venues", controllers.GetAllVenue)
	r.GET("/venues/:id", controllers.GetVenueById)
	jwt.POST("/venues", controllers.PostVenue)
	jwt.PUT("/venues/:id", controllers.EditVenue)
	jwt.DELETE("/venues/:id", controllers.DeleteVenue)

	// Tournament Routes
	r.GET("/events", controllers.GetAllEvents)
	r.GET("/events/:id", controllers.GetEventById)
	jwt.POST("/events", controllers.PostEvents)
	jwt.DELETE("/events/:id", controllers.DeleteEvents)

	// Event Cart Routes
	r.GET("/events/cart/:id", controllers.GetCartByParamEventId)
	jwt.GET("/events/cart", controllers.GetCartByTokenId)
	jwt.POST("/events/cart", controllers.PostCart)
	jwt.DELETE("/events/cart/:id", controllers.DeleteCart)

	return r
}
