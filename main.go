package main

import (
	"fmt"
	"sparingapp/configs"
	"sparingapp/routes"
)

func main() {
	// connect with database
	db := configs.ConnectDataBase()
	sqlDB, _ := db.DB()

	defer sqlDB.Close()

	// running router
	r := routes.SetupRouter(db)
	r.Run()

	fmt.Println("Server is running...")
}
